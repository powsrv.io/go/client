package powsrvio

import (
	"crypto/tls"
	"errors"
	"fmt"
	"io"
	"sync"
	"sync/atomic"
	"time"

	"github.com/iotaledger/iota.go/trinary"
	"gitlab.com/powsrv.io/go/api"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/keepalive"
	"google.golang.org/grpc/metadata"
)

const (
	serverName    string = "api.powsrv.io"
	serverAddress string = "api.powsrv.io:443"
)

// Errors returned by the client
var (
	ErrClosed         = errors.New("client closed")
	ErrNotInitialized = errors.New("client not initialized")
	ErrReceiveTimeout = errors.New("receive timeout")
)

// PowClient is the client that connects to the powsrv.io
type PowClient struct {
	ReadTimeOutMs   int64                // timeout in ms to receive a response
	APIKey          string               // API key for powsrv.io
	Verbose         bool                 // print info messages
	connection      *grpc.ClientConn     // connection to the powsrv.io
	powStream       api.Pow_DoPowClient  // stream for DoPow
	connectionError error                // reason for a closed connection
	requestID       uint64               // sequence number of the request
	requestQueue    chan *PendingRequest // outgoing PendingRequests to the powsrv.io
	pending         PendingRequests      // pending requests by ID

	wg          sync.WaitGroup
	closing     chan struct{}
	closingOnce sync.Once
}

// PendingRequest contains the information about a request and the response and error channel for signaling
type PendingRequest struct {
	powRequest      *api.PowRequest
	powResponseChan chan *api.PowResponse
	errorChan       chan error
}

// Init initializes a PowClient and connects to the server.
func (client *PowClient) Init() error {
	var err error

	creds := credentials.NewTLS(&tls.Config{
		ServerName: serverName,
	})
	kacp := keepalive.ClientParameters{
		Time:                30 * time.Second,
		Timeout:             5 * time.Second,
		PermitWithoutStream: true,
	}

	client.connectionError = nil
	client.closingOnce = sync.Once{}

	// set up a connection to the server.
	client.connection, err = grpc.Dial(serverAddress, grpc.WithTransportCredentials(creds), grpc.WithKeepaliveParams(kacp))
	if err != nil {
		return err
	}

	client.closing = make(chan struct{})
	client.requestQueue = make(chan *PendingRequest)
	client.pending = PendingRequests{
		byID: make(map[uint64]*PendingRequest),
	}

	// set up the proper context for the gRPC stream
	ctx, cancel := context.WithCancel(context.Background())
	md := metadata.Pairs("authorization", "powsrv-token "+client.APIKey)
	mdContext := metadata.NewOutgoingContext(ctx, md)

	// set up a RPC connection to the server.
	rpcClient := api.NewPowClient(client.connection)
	client.powStream, err = rpcClient.DoPow(mdContext)
	if err != nil {
		return err
	}

	client.wg.Add(2)
	go client.recvLoop(cancel)
	go client.sendLoop()

	return nil
}

// Close tears down the PowClient and all underlying connections.
// It is not safe to call Close concurrently with PowFunc.
func (client *PowClient) Close() {
	client.closingOnce.Do(func() {
		if client.closing != nil {
			close(client.closing)
		}
		if client.requestQueue != nil {
			close(client.requestQueue)
		}
		if client.connection != nil {
			client.connection.Close()
		}
		client.wg.Wait()
		client.connection = nil
	})
}

func (client *PowClient) printf(format string, a ...interface{}) {
	if client.Verbose {
		fmt.Printf(format, a...)
	}
}

func (client *PowClient) recvLoop(cancel context.CancelFunc) {
	defer client.wg.Done()
	defer cancel() // always cancel the powStream when receiving stops

	for {
		res, err := client.powStream.Recv()
		if err != nil {
			client.printf("Error receiving response: %s\n", err)
			client.connectionError = err
			client.pending.cancel(err)
			return
		}
		client.printf("Received response: %v\n", res)
		client.pending.setResponse(res)
	}
}

func (client *PowClient) sendLoop() {
	defer client.wg.Done()
	defer client.powStream.CloseSend()

	for request := range client.requestQueue {
		client.pending.add(request)
		err := client.powStream.Send(request.powRequest)
		if client.connectionError != nil && errors.Is(err, io.EOF) {
			err = client.connectionError
		}
		if err != nil {
			client.printf("Error sending request: %s\n", err)
			select { // send error only when channel is not full
			case request.errorChan <- err:
			default:
			}
		}
	}
}

// PowFunc does the POW by sending a request to the powsrv.io server.
// It is not safe to call Close concurrently with PowFunc.
func (client *PowClient) PowFunc(trytes trinary.Trytes, minWeightMagnitude int, parallelism ...int) (trinary.Trytes, error) {
	if client.connection == nil {
		return "", ErrNotInitialized
	}
	if (minWeightMagnitude < 1) || (minWeightMagnitude > 14) {
		return "", fmt.Errorf("minWeightMagnitude out of range [1-14]: %v", minWeightMagnitude)
	}

	// get the next request ID
	reqID := atomic.AddUint64(&client.requestID, 1)

	// prepare a request
	apiRequest := &api.PowRequest{
		Identifier: reqID,
		Trytes:     trytes,
		Mwm:        uint32(minWeightMagnitude),
	}
	pendingPowRequest := &PendingRequest{
		powRequest:      apiRequest,
		powResponseChan: make(chan *api.PowResponse, 1),
		errorChan:       make(chan error, 1),
	}

	// this might panics, when PowFunc was called concurrently with Close
	client.requestQueue <- pendingPowRequest
	// always remove the request
	defer func() { client.pending.delete(reqID) }()

	select {
	case powResponse := <-pendingPowRequest.powResponseChan:
		return trinary.NewTrytes(powResponse.Nonce)

	case err := <-pendingPowRequest.errorChan:
		return "", err

	case <-time.After(time.Duration(client.ReadTimeOutMs) * time.Millisecond):
		return "", ErrReceiveTimeout

	case <-client.closing:
		return "", ErrClosed
	}
}

// PendingRequests stores all pending request and matches responses to the corresponding request.
type PendingRequests struct {
	sync.Mutex
	byID map[uint64]*PendingRequest
}

func (r *PendingRequests) add(req *PendingRequest) {
	r.Lock()
	defer r.Unlock()

	r.byID[req.powRequest.Identifier] = req
}

func (r *PendingRequests) delete(reqID uint64) {
	r.Lock()
	defer r.Unlock()

	delete(r.byID, reqID)
}

func (r *PendingRequests) cancel(err error) {
	r.Lock()
	defer r.Unlock()

	for id, req := range r.byID {
		select { // send error only when channel is not full
		case req.errorChan <- err:
		default:
		}
		delete(r.byID, id)
	}
}

func (r *PendingRequests) setResponse(res *api.PowResponse) {
	r.Lock()
	defer r.Unlock()

	pendingPowRequest, ok := r.byID[res.Identifier]
	if ok {
		pendingPowRequest.powResponseChan <- res
	}
}
