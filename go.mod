module gitlab.com/powsrv.io/go/client

go 1.14

require (
	github.com/iotaledger/iota.go v1.0.0-beta.15
	gitlab.com/powsrv.io/go/api v0.0.0-20180825222338-acd4e4dcc0bf
	golang.org/x/net v0.0.0-20200707034311-ab3426394381
	google.golang.org/grpc v1.30.0
)
