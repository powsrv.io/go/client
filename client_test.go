package powsrvio

import (
	"context"
	"errors"
	"flag"
	"io"
	"math/rand"
	"strings"
	"sync"
	"testing"
	"time"

	"github.com/iotaledger/iota.go/consts"
	"github.com/iotaledger/iota.go/curl"
	"github.com/iotaledger/iota.go/guards"
	"github.com/iotaledger/iota.go/pow"
	"github.com/iotaledger/iota.go/trinary"
	"gitlab.com/powsrv.io/go/api"
	"google.golang.org/grpc/metadata"
)

var (
	testMWM               = 6
	nullTransactionTrytes = strings.Repeat("9", consts.TransactionTrytesSize)

	apiKey = flag.String("key", "", "optional powsrv.io API key to test against powsrv.io server")
)

func testPowFunc(t *testing.T, client *PowClient) {
	trytes := trinary.IntToTrytes(rand.Int63(), consts.TransactionTrytesSize)
	nonce, err := client.PowFunc(trytes, testMWM)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
		return
	}
	hash, err := curl.HashTrytes(trytes[:consts.NonceTrinaryOffset/3] + nonce)
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
		return
	}
	zeros := trinary.TrailingZeros(trinary.MustTrytesToTrits(hash))
	if zeros < testMWM {
		t.Errorf("Unexpected mwm: expected=%d go=%d", testMWM, zeros)
	}
}

func TestPOW(t *testing.T) {
	const (
		workerCount = 4
		testCount   = 10
	)

	powClient := newTestClient(t)
	defer powClient.Close()

	var wg sync.WaitGroup
	wg.Add(workerCount)
	for worker := 0; worker < workerCount; worker++ {
		go func() {
			defer wg.Done()
			for i := 0; i < testCount; i++ {
				testPowFunc(t, powClient)
			}
		}()
	}

	wg.Wait()
}

func TestClose(t *testing.T) {
	powClient := newTestClient(t)
	powClient.Close()
}

func TestCloseTwice(t *testing.T) {
	powClient := newTestClient(t)
	powClient.Close()
	powClient.Close()
}

func TestSendAfterClose(t *testing.T) {
	powClient := newTestClient(t)
	powClient.Close()

	_, err := powClient.PowFunc(nullTransactionTrytes, testMWM)
	if !errors.Is(err, ErrNotInitialized) {
		t.Fatal(err)
	}
}

// signalSentMock signals when a send was performed.
type signalSentMock struct {
	api.Pow_DoPowClient
	sent chan struct{}
}

func (m *signalSentMock) Send(req *api.PowRequest) error {
	err := m.Pow_DoPowClient.Send(req)
	m.sent <- struct{}{}
	return err
}

func TestCloseWhileReceiving(t *testing.T) {
	powClient := newTestClient(t)
	signalMock := &signalSentMock{
		Pow_DoPowClient: powClient.powStream,
		sent:            make(chan struct{}, 1),
	}
	powClient.powStream = signalMock

	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()
		_, err := powClient.PowFunc(nullTransactionTrytes, testMWM)
		if !errors.Is(err, ErrClosed) {
			t.Fatal(err)
		}
	}()

	<-signalMock.sent
	powClient.Close()
	wg.Wait()
}

// sendErrorMock always returns an error on send.
type sendErrorMock struct {
	api.Pow_DoPowClient
	err error
}

func (m *sendErrorMock) Send(_ *api.PowRequest) error {
	return m.err
}

func TestSendError(t *testing.T) {
	powClient := newTestClient(t)
	errorMock := &sendErrorMock{
		Pow_DoPowClient: powClient.powStream,
		err:             errors.New("send error"),
	}
	powClient.powStream = errorMock
	defer powClient.Close()

	_, err := powClient.PowFunc(nullTransactionTrytes, testMWM)
	if !errors.Is(err, errorMock.err) {
		t.Fatalf("unexpected error: %v", err)
	}

	powClient.powStream = errorMock.Pow_DoPowClient
	testPowFunc(t, powClient)
}

// recvErrorMock always returns an error on Recv.
type recvErrorMock struct {
	api.Pow_DoPowClient
	err error
}

func (m *recvErrorMock) Recv() (*api.PowResponse, error) {
	_, _ = m.Pow_DoPowClient.Recv()
	return nil, m.err
}

func TestRecvError(t *testing.T) {
	var recvErr = errors.New("receive error")

	powClient := newTestClient(t)
	errorMock := &recvErrorMock{
		Pow_DoPowClient: powClient.powStream,
		err:             recvErr,
	}
	powClient.powStream = errorMock
	defer powClient.Close()

	_, err := powClient.PowFunc(nullTransactionTrytes, testMWM)
	if !errors.Is(err, errorMock.err) {
		t.Fatalf("unexpected error: %v", err)
	}

	errorMock.err = nil
	_, err = powClient.PowFunc(nullTransactionTrytes, testMWM)
	if !errors.Is(err, recvErr) {
		t.Fatalf("unexpected error: %v", err)
	}
}

// recvDelayMock delays Recv by the given time.
type recvDelayMock struct {
	api.Pow_DoPowClient
	delay time.Duration
}

func (m *recvDelayMock) Recv() (*api.PowResponse, error) {
	start := time.Now()
	res, err := m.Pow_DoPowClient.Recv()
	time.Sleep(m.delay - time.Since(start))
	return res, err
}

func TestTimeout(t *testing.T) {
	powClient := newTestClient(t)
	timeoutMock := &recvDelayMock{
		Pow_DoPowClient: powClient.powStream,
		delay:           time.Duration(powClient.ReadTimeOutMs+10) * time.Millisecond,
	}
	powClient.powStream = timeoutMock
	defer powClient.Close()

	_, err := powClient.PowFunc(nullTransactionTrytes, testMWM)
	if !errors.Is(err, ErrReceiveTimeout) {
		t.Fatalf("unexpected error: %v", err)
	}

	timeoutMock.delay = 0
	testPowFunc(t, powClient)
}

func TestKeepAlive(t *testing.T) {
	powClient := newTestClient(t)
	defer powClient.Close()

	testPowFunc(t, powClient)
	time.Sleep(61 * time.Second)
	testPowFunc(t, powClient)
}

func newTestClient(t *testing.T) *PowClient {
	powClient := &PowClient{ReadTimeOutMs: 5000, Verbose: true, APIKey: *apiKey}
	err := powClient.Init()
	if err != nil {
		t.Fatal(err)
	}
	// mock if no api key was supplied
	if len(*apiKey) == 0 {
		powClient.powStream = &clientStreamMock{
			ctx:      powClient.powStream.Context(),
			requests: make(chan *api.PowRequest),
		}
	}
	return powClient
}

// clientStreamMock mocks Pow_DoPowClient to do offline tests.
type clientStreamMock struct {
	ctx      context.Context
	requests chan *api.PowRequest
}

func (m *clientStreamMock) Send(req *api.PowRequest) error {
	if len(req.Trytes) != consts.TransactionTrytesSize || !guards.IsTrytes(req.Trytes) {
		return consts.ErrInvalidTrytes
	}
	select {
	case m.requests <- req:
	case <-m.ctx.Done():
		return io.EOF
	}
	return nil
}

// Recv returns the first trytes of the last request as a nonce.
func (m *clientStreamMock) Recv() (*api.PowResponse, error) {
	var req *api.PowRequest
	select {
	case req = <-m.requests:
	case <-m.ctx.Done():
	}
	if req == nil {
		return nil, io.EOF
	}
	nonce, err := pow.SyncGoProofOfWork(req.Trytes, testMWM, 1)
	if err != nil {
		return nil, err
	}
	res := &api.PowResponse{
		Identifier: req.Identifier,
		Nonce:      nonce,
	}
	return res, nil
}

// CloseSend closes the request channel.
func (m *clientStreamMock) CloseSend() error {
	close(m.requests)
	return nil
}

func (m *clientStreamMock) Header() (metadata.MD, error) { panic("not implemented") }
func (m *clientStreamMock) Trailer() metadata.MD         { panic("not implemented") }
func (m *clientStreamMock) Context() context.Context     { panic("not implemented") }
func (m *clientStreamMock) SendMsg(_ interface{}) error  { panic("not implemented") }
func (m *clientStreamMock) RecvMsg(_ interface{}) error  { panic("not implemented") }
